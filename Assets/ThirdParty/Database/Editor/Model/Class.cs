﻿using UnityEngine;

using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Class
	{
		public string Name;

		public List<Field> Fields;

		public Class()
		{
			Fields = new List<Field>();
		}

		public Class(string name)
		{
			Name = name;
			Fields = new List<Field>();
		}

		public Class(Class source)
		{
			Name = source.Name;
			Fields = new List<Field>();
			foreach (Field field in source.Fields)
			{
				Fields.Add(new Field(field));
			}
		}
	}
}
