﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("GameObject", "Unity", typeof(GameObject), "GameObject", "null")]
	public class GameObjectField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (GameObject)item, typeof(GameObject), false);
			return item;
		}
	}
}
