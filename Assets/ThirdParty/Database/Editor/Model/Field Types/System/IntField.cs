﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Integer", "System", typeof(int), "int", "0")]
	public class IntField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.IntField((item == null) ? 0 : (int)item, GUILayout.MinWidth(0f));
			return item;
		}

		public override object GetDefaultValue()
		{
			return 0;
		}
	}
}
