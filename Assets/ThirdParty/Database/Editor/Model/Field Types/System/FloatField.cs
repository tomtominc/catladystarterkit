﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Float", "System", typeof(float), "float", "0f")]
	public class FloatField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.FloatField((item == null) ? 0f : (float)item, GUILayout.MinWidth(0f));
			return item;
		}

		public override object GetDefaultValue()
		{
			return 0f;
		}
	}
}
