﻿using System;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	public abstract class FieldType
	{
		public virtual void DrawField(BaseClass item, string fieldName)
		{
			PropertyInfo propertyInfo = item.GetType().GetProperty(fieldName);
			if (propertyInfo != null)
			{
				object value = propertyInfo.GetValue(item, null);
				Type valueType = EditorUtil.GetFieldTypeType(this.GetType());
				value = DrawField((value == null) ? null : Convert.ChangeType(value, valueType));
				propertyInfo.SetValue(item, value, null);
			}
		}

		public virtual object GetDefaultValue()
		{
			return null;
		}

		public abstract object DrawField(object item);
	}

	public class FieldInfoAttribute : Attribute
	{
		public FieldInfoAttribute(string name, string category, Type type, string typeString, string defaultValue, string requiredNamespace = "")
		{
			this.Category = category;
			this.Name = name;
			this.Type = type;
			this.TypeString = typeString;
			this.DefaultValue = defaultValue;
			this.RequiredNamespace = requiredNamespace;
		}

		public string Name { get; set; }

		public string Category { get; set; }

		public Type Type { get; set; }

		public string TypeString { get; set; }

		public string DefaultValue { get; set; }

		public string RequiredNamespace { get; set; }
	}
}
