using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseCatBlueprint : BaseClass
	{

		[SerializeField]
		protected string m_Description = string.Empty;

		public virtual string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}

		[SerializeField]
		protected Location m_Location = Location.House;

		public virtual Location Location
		{
			get { return m_Location; }
			set { m_Location = value; }
		}

		[SerializeField]
		protected float m_ProductionPerSecond = 0f;

		public virtual float ProductionPerSecond
		{
			get { return m_ProductionPerSecond; }
			set { m_ProductionPerSecond = value; }
		}

		[SerializeField]
		protected float m_ProductionMaxUpdate = 0f;

		public virtual float ProductionMaxUpdate
		{
			get { return m_ProductionMaxUpdate; }
			set { m_ProductionMaxUpdate = value; }
		}

		[SerializeField]
		protected float m_BaseShopCoinCost = 0f;

		public virtual float BaseShopCoinCost
		{
			get { return m_BaseShopCoinCost; }
			set { m_BaseShopCoinCost = value; }
		}

		[SerializeField]
		protected int m_BaseShopPremiumCost = 0;

		public virtual int BaseShopPremiumCost
		{
			get { return m_BaseShopPremiumCost; }
			set { m_BaseShopPremiumCost = value; }
		}

		[SerializeField]
		protected int m_Purchased = 0;

		public virtual int Purchased
		{
			get { return m_Purchased; }
			set { m_Purchased = value; }
		}

		[SerializeField]
		protected bool m_Unlocked = false;

		public virtual bool Unlocked
		{
			get { return m_Unlocked; }
			set { m_Unlocked = value; }
		}

		[SerializeField]
		protected GameObject m_Prefab = null;

		public virtual GameObject Prefab
		{
			get { return m_Prefab; }
			set { m_Prefab = value; }
		}

	}
}
