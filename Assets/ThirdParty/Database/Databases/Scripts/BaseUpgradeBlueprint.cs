using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseUpgradeBlueprint : BaseClass
	{

		[SerializeField]
		protected string m_Description = string.Empty;

		public virtual string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}

		[SerializeField]
		protected float m_BaseCost = 0f;

		public virtual float BaseCost
		{
			get { return m_BaseCost; }
			set { m_BaseCost = value; }
		}

		[SerializeField]
		protected float m_BasePremiumCost = 0f;

		public virtual float BasePremiumCost
		{
			get { return m_BasePremiumCost; }
			set { m_BasePremiumCost = value; }
		}

		[SerializeField]
		protected string m_Suffix = string.Empty;

		public virtual string Suffix
		{
			get { return m_Suffix; }
			set { m_Suffix = value; }
		}

		[SerializeField]
		protected GameObject m_Icon = null;

		public virtual GameObject Icon
		{
			get { return m_Icon; }
			set { m_Icon = value; }
		}

		[SerializeField]
		protected int m_MinUpgrade = 0;

		public virtual int MinUpgrade
		{
			get { return m_MinUpgrade; }
			set { m_MinUpgrade = value; }
		}

		[SerializeField]
		protected int m_MaxUpgrade = 0;

		public virtual int MaxUpgrade
		{
			get { return m_MaxUpgrade; }
			set { m_MaxUpgrade = value; }
		}

		[SerializeField]
		protected int m_Step = 0;

		public virtual int Step
		{
			get { return m_Step; }
			set { m_Step = value; }
		}

		[SerializeField]
		protected int m_CurrentUpgrade = 0;

		public virtual int CurrentUpgrade
		{
			get { return m_CurrentUpgrade; }
			set { m_CurrentUpgrade = value; }
		}

	}
}
