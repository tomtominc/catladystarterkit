﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using RutCreate.LightningDatabase;
using Framework.Core.EventSystem;

public class CatStoreManager : Loader
{
    public Transform _catDummys;
    public Transform _catItemPrefab;


    public override IEnumerator Load()
    {
        foreach (CatBlueprint blueprint in CatDex.GetAllCats())
        {
            Transform item = Instantiate < Transform >(_catItemPrefab);
            item.SetParent(transform, false);
            CatStoreItem storeItem = item.GetComponent < CatStoreItem >();
            storeItem.Initialize(this, blueprint, _catDummys);
        }
        yield break;
    }

    public void Purchase(CatBlueprint blueprint, float cost)
    {
        if (StatsManager.Instance.Gold < cost)
            return;

        EventManager.Publish(this, ScoreEvent.Gold, -cost);

        blueprint.Purchased++;

        PersistentData spawnData = new PersistentData(blueprint.Name, blueprint.Location, Vector3.zero);
    }

    public void PurchasePremium(CatBlueprint blueprint, int cost)
    {
        
    }
}
