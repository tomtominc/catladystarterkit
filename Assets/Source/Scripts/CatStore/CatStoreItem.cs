﻿using UnityEngine;
using System.Collections;
using RutCreate.LightningDatabase;
using UnityEngine.UI;
using Framework.Core;

public class CatStoreItem : MonoBehaviour
{
    private CatStoreManager _catStoreManager;
    private CatBlueprint _catBlueprint;

    [SerializeField]private CatDexIcon _catIcon;
    [SerializeField]private Text _titleLabel;
    [SerializeField]private Text _descriptionLabel;
    [SerializeField]private Text _purchasedInfoLabel;
    [SerializeField]private Text _costLabel;
    [SerializeField]private Text _premiumCostLabel;

    public void Initialize(CatStoreManager catStoreManager, CatBlueprint catBlueprint, Transform catDummies)
    {
        _catStoreManager = catStoreManager;
        _catIcon._dummyCatParent = catDummies;

        Debug.Log(_catIcon._dummyCatParent);
        _catIcon.Init();
        RefreshGUI(catBlueprint);
    }

    private void RefreshGUI(CatBlueprint catBlueprint)
    {
        _catBlueprint = catBlueprint;

        _catIcon.Set(_catBlueprint);
        _titleLabel.text = _catBlueprint.Name;
        _descriptionLabel.text = _catBlueprint.Description;
        _purchasedInfoLabel.text = string.Format("Purchased: {0}", _catBlueprint.Purchased);
        _costLabel.text = string.Format("{0:N}", GetCost());
        _premiumCostLabel.text = string.Format("{0:N}", _catBlueprint.BaseShopPremiumCost + (_catBlueprint.BaseShopPremiumCost * _catBlueprint.Purchased));
    }

    public float GetCost()
    {
        return  _catBlueprint.BaseShopCoinCost + Mathf.Pow(_catBlueprint.BaseShopCoinCost, _catBlueprint.Purchased);
    }

    public void Purchase_Button()
    {
        _catStoreManager.Purchase(_catBlueprint, GetCost());
        RefreshGUI(_catBlueprint);
    }

    public void PurchasePremium_Button()
    {
        
    }

   
}
