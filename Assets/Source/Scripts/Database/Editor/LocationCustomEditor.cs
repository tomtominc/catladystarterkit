﻿using UnityEngine;
using UnityEditor;

namespace RutCreate.LightningDatabase
{
    [FieldInfo("Location", "Custom", typeof(Location), "Location", "Location.House")]
    public class LocationCustomEditor : FieldType
    {
        public override object DrawField(object item)
        {
            Location value = (item == null) ? Location.House : (Location)item;
            value = (Location)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return Location.House;
        }
    }
}