﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Framework.Core.EventSystem;
using System.Linq;
using PathologicalGames;
using Framework.Utilities;

#if ENABLE_GRIDS
using Gamelogic.Grids;

namespace Framework.Core
{
    [System.Serializable]
    public class GridColor
    {
        public Vector2 startPoint;
        public Vector2 endPoint;
        public Color color;
    }

    public class GridManager : Singleton < GridManager > , ILoader
	{

        public GridBehaviour Grid;

        public static string SPAWN_CATEGORY = "Behaviours";

        public string prefabPath = "Behaviours";

        public string _cellDataPath;

        public LayerMask cellLayer;
       
        [SerializeField]
        private float _priority;

        public AnimationCurve difficultyCurve;


        public float Priority { get { return _priority; } }

        private Dictionary < Difficulty , List < EntityGroup > > _groups;

        public Dictionary < Difficulty, List < EntityGroup > > Groups
        {
            get 
            {
                if ( _groups == null )
                {
                    LoadManual ();
                }

                return _groups;
            }
        }

        public IEnumerator Load ()
        {
            LoadManual ();

            //Grid
            yield break;
        }

        public void LoadManual ()
        {
            List < EntityGroup > hazards = PersistentManager.LoadFiles < EntityGroup > ( _cellDataPath );

            _groups = new Dictionary<Difficulty, List < EntityGroup > > ();

            if ( hazards.Count > 0 )
            {
                foreach ( var hazard in hazards )
                {
                    if ( _groups.ContainsKey ( hazard.difficulty ) )
                    {
                        _groups [ hazard.difficulty ].Add ( hazard.Clone () );
                    }
                    else
                    {
                        _groups.Add ( hazard.difficulty, new List < EntityGroup > () { hazard.Clone () });
                    }
                }
            }

            EventManager.Subscribe ( ApplicationEvent.OnChangedApplicationState , OnChangeApplicationState );

        }

        private void OnChangeApplicationState  ( IMessage message )
        {
            ApplicationState state = (ApplicationState)message.Data;

            if ( state == ApplicationState.InGame && _groups.Count > 0 && _groups.ContainsKey ( Difficulty.Start ) )
            {

                EntityGroup selectedGroup = GetRandomHazard ( Difficulty.Start );

                //foreach ( var hazard in HazardGroup )
            }
        }

        public EntityGroup GetRandomHazard ( Difficulty difficulty )
        {
            Selector selector = new Selector ();

            return selector.Single ( _groups [ difficulty ] );
        }

        public void SpawnCellGroup ()
        {
            
        }

        public static void Despawn ( Transform entity )
        {
            PoolManager.Pools [ SPAWN_CATEGORY ].Despawn ( entity );
        }

        public static IEntity Spawn ( string prefab  , Vector2 point )
        {
            Transform clone = PoolManager.Pools[SPAWN_CATEGORY].Spawn ( prefab );

            if ( clone == null ) return null;

            IEntity entity = clone.GetComponent < IEntity > ();

            if ( entity != null )
            {
                Instance.Grid.Spawn ( entity, point );
            }

            return entity;
        }

        public void SpawnCell ( EntityData data )
        {
            Transform entity = PoolManager.Pools [SPAWN_CATEGORY].Spawn ( data.key );
        }

        public void SaveGroup ()
        {
            
        }
            
        public void SaveAllGroups ()
        {
            Debug.Log ( "Save All groups called" );   
        }

        private void Update ()
        {
            if ( ___editorMode )
            {
                ___EditorUpdate ( ___editorGroup );
            }
        }

        [HideInInspector]
        public bool ___editorMode = false;

        [HideInInspector]
        public EntityGroup ___editorGroup;

        public void ___EditorUpdate ( EntityGroup group )
        {
            if ( group == null ) return;

            if ( Input.GetMouseButtonDown (0) )
            {
                RaycastHit2D info = Raycast.Cursor ( cellLayer );

                if ( info.transform != null )
                {
                    Spawn ( group.prefab, Grid.Map [ info.point ].ToV2 () );  
                }
            }
        }
	}
}

#endif