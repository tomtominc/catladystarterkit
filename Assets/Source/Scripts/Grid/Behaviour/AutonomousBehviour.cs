﻿using UnityEngine;
using System.Collections;

#if ENABLE_PANDA
using Panda;
#endif

namespace Framework.Core
{
    public class AutonomousBehviour : EntityBehaviour 
	{
        #if ENABLE_PANDA
        [Header ("Autonomous Properties")]

        public LayerMask lookMask;
        public Vector2 lookDirection;
        public Vector2 stepLimit;
        public bool teleport;

        [Task]
        private bool canSeePlayer = false;

        [Task]
        private void FindPlayer ()
        {
            //VectorLine.SetRay ( Color.yellow, 0.1f, transform.position , lookDirection * 3f );

            RaycastHit2D info = Physics2D.Raycast ( transform.position, lookDirection, Mathf.Infinity, lookMask );

            canSeePlayer = info.transform != null;

            Task.current.Succeed ();
        }

        [Task]
        private void MoveTowardsPlayer ()
        {
            bool initial = Task.current.isStarting;

            if ( initial )
            {
                IEntity player = _world.Find < CharacterBehaviour > ();

                Vector2 distanceFromPlayer = player.Transform.position - transform.position;

                Vector2 moveDistance = distanceFromPlayer.Clamp ( stepLimit );

                if ( Move ( moveDistance, teleport, null ))
                {
                    Task.current.Succeed ();
                }
                else
                {
                    Task.current.Fail ();
                }
            }
        }

        [Task]
        private void SpawnEntity ()
        {
            Task.current.Succeed ();
        }

        #endif
	}
}