﻿using UnityEngine;
using System.Collections;
using Framework.Core.Animation;
using System;
using Framework.EditorTools;

namespace Framework.Core
{
    public enum EntityEvent
    {
        OnDamage, OnPickup
    }

    public class EntityBehaviour : MonoBehaviour, IEntity
	{
        [Header("Base Behaviour Options")]
        [SerializeField]
        protected Vector2 _offsetPosition = Vector2.zero;
        [SerializeField]
        protected Color _occupiedColor = Color.white;

        public Rect boundries = new Rect (-1,-1,-1,-1);


        public virtual string Name { get { return name; } }
        public virtual Transform Transform { get { return transform; } }
        public virtual SpriteRenderer SpriteRenderer { get { return _renderer; } }
        public virtual SpriteAnimation Animator { get { return _animator; } }
        public virtual Vector2 CurrentPoint { get { return _currentPoint; } }
        public virtual Vector2 OffsetPosition { get { return _offsetPosition; } }
        public virtual Color OccupiedColor { get { return _occupiedColor; } }

        protected IWorldHandler _world;

        protected Transform _visualization;
        protected SpriteRenderer _renderer;
        protected SpriteAnimation _animator;
        protected Vector2 _currentPoint;

        public void Initialize ( IWorldHandler world )
        {
            _world = world;

            _visualization = transform.FindChild ( "Visualization" );

            if ( _visualization == null )
            {
                Debug.LogWarning ( "You have no child object called 'Visualization', " +
                    "please place a sprite renderer and sprite animation component on this object." );
                return;
            }

            _renderer = _visualization.GetComponent < SpriteRenderer  > ();
            _animator = _visualization.GetComponent < SpriteAnimation > ();

            Enabled ();
        }

        private void OnDisable ()
        {
            Disabled ();
        }

        public virtual void OnEnter ( Vector2 point )
        {
            _currentPoint = point;
        }

        public virtual void OnExit ()
        {
            
        }

        private void Update ()
        {
            OnUpdate ();
        }

        protected virtual void OnUpdate ()
        {
            
        }

        public bool Move ( Vector2 offset, bool instant, Action < Vector2 > onComplete )
        {
            Vector2 movePoint = _currentPoint + offset;

            if ( boundries.x < 0 || boundries.Contains ( movePoint ))
            {
                return _world.Move ( ((IEntity)this) , instant, _currentPoint , movePoint , 0.1f , onComplete );
            }

            return false;
                
        }

        // virtual methods not used in the base class except to call them for child classes
        public virtual void Enabled () { }
        public virtual void Disabled () { }

        public virtual void RemoveFromWorld () 
        { 
            _world.Despawn ( ((IEntity)this) );
        }


	}
}