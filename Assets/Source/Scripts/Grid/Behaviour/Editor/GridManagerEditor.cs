﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using PathologicalGames;

#if ENABLE_GRIDS
namespace Framework.Core
{
    
    [CustomEditor(typeof(GridManager))]
    public class GridManagerEditor : Editor 
	{
        private GridManager grid;
        private Difficulty difficulty;
        private int prefabIndex;

        private Dictionary < Difficulty, List < EntityGroup > > editorGroups;

        private GUIStyle background;
        private GUIStyle poolBackground;
        private GUIStyle dropBox;


        private string searchStr = "";

        private void OnEnable()
        {
            grid = (GridManager)target;

            Load ();

            background = new GUIStyle();
            poolBackground = new GUIStyle();
            dropBox = new GUIStyle();

            background.normal.background = MakeTex(new Color(0.5f, 0.5f, 0.5f, 0.5f));
            poolBackground.normal.background = MakeTex(new Color(0.3f, 0.3f, 0.3f, 0.5f));
            dropBox.normal.background = MakeTex(new Color(1, 1, 1, 0.5f));

            poolBackground.margin = new RectOffset(2, 2, 2, 2);
            dropBox.margin = new RectOffset(4, 4, 4, 4);

            dropBox.alignment = TextAnchor.MiddleCenter;

            dropBox.fontSize = 14;

            dropBox.normal.textColor = Color.black;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector ();

            EditorGUILayout.Space ();
            EditorGUILayout.Space ();

            if (editorGroups == null )
                Load ();

            grid.___editorMode = EditorGUIExtensions.ToggleTitle ( "Editor Mode", "GRID_BEHAVIOUR_EDITOR_MODE", Color.white );

            if (  grid.___editorMode )
            {
                if ( !Application.isPlaying )
                {
                    EditorGUILayout.HelpBox ( "Need to be in playmode to use the editor", MessageType.Info );
                }
                else
                {
                    EditorGUIExtensions.BeginContent (Color.white);
                    Toolbar ();
                    DrawGroups ();
                    OptionsBox ();
                    EditorGUIExtensions.EndContent ();
                }
               
            }
        }

        private void DrawGroups ()
        {
            foreach ( KeyValuePair < Difficulty, List < EntityGroup > > group in editorGroups )
            {
                DrawGroupHeader ( group );
            }
        }

        private void Toolbar()
        {
            GUILayout.BeginHorizontal(GUI.skin.FindStyle("Toolbar"), GUILayout.ExpandWidth(true));
            GUILayout.Label( "Entity Groups");
            SearchField();

            GUILayout.FlexibleSpace();

            difficulty = (Difficulty)EditorGUILayout.EnumPopup ( difficulty, EditorStyles.toolbarDropDown );

            if (GUILayout.Button("+", EditorStyles.toolbarButton, GUILayout.Width(15)))
            {
                AddGroup ( difficulty, new EntityGroup () { difficulty = difficulty } );
            }

            GUILayout.EndHorizontal();
        }

        private void DrawGroupHeader ( KeyValuePair < Difficulty, List < EntityGroup > > group )
        {
            GUILayout.BeginVertical(poolBackground);
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.Space(10f);

            string key = string.Format ("ENTITY_FOLDOUT_{0}", group.Key );

            bool foldout = EditorGUILayout.Foldout( EditorPrefs.GetBool ( key,false ) , group.Key.ToString () );

            EditorPrefs.SetBool ( key , foldout );

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("+", EditorStyles.toolbarButton, GUILayout.Width(15)))
            {
                AddGroup ( group.Key, new EntityGroup () { difficulty = group.Key } );
            }

            GUILayout.EndHorizontal();

            if ( foldout )
            {  
                foreach ( var hazard in group.Value )
                {
                    DrawEntityArea ( hazard );
                }
            }

            GUILayout.EndVertical();
          
        }

        private void DrawEntityArea ( EntityGroup group )
        {
            if ( grid.___editorGroup == null || grid.___editorGroup.id != group.id )
            {
                EditorGUIExtensions.SetToggleButton ( group.id.ToString () , false );
            }

            if ( EditorGUIExtensions.ToggleButton ( group.name, group.id.ToString () , "Button", EditorColor.blue ))
            {
                grid.___editorGroup = group.Clone ();

                group.name = EditorGUILayout.TextField ( "Name", group.name );
                group.difficulty = (Difficulty)EditorGUILayout.EnumPopup ( "Difficulty", group.difficulty );
                group.selectionWeight = EditorGUILayout.FloatField ( "Weight", group.selectionWeight );

                if ( PoolManager.Pools.ContainsKey ( GridManager.SPAWN_CATEGORY) && PoolManager.Pools [ GridManager.SPAWN_CATEGORY ].prefabs != null && PoolManager.Pools [GridManager.SPAWN_CATEGORY ].prefabs.Count > 0 )
                {
                    var prefabs = PoolManager.Pools[GridManager.SPAWN_CATEGORY].prefabs.Select ( x => x.Value.name ).ToArray ();

                    if ( !string.IsNullOrEmpty ( group.prefab ) )
                    {
                        prefabIndex = prefabs.ToList ().FindIndex ( x => x == group.prefab );
                    }

                    prefabIndex = EditorGUILayout.Popup ( "Prefab", prefabIndex, prefabs );

                    group.prefab = prefabs [ prefabIndex ];
                }

                EditorGUILayout.BeginHorizontal ();

                if ( GUILayout.Button ( "Save" ) )
                {

                }
                if ( GUILayout.Button ( "Save As..." ) )
                {

                }

                if ( GUILayout.Button ( "Delete" ) )
                {

                }

                EditorGUILayout.EndHorizontal ();
            }
        }

        private void SearchField()
        {

            searchStr = GUILayout.TextField(searchStr, GUI.skin.FindStyle("ToolbarSeachTextField"), GUILayout.ExpandWidth(true), GUILayout.MinWidth(150));
            if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSeachCancelButton")))
            {
                // Remove focus if cleared
                searchStr = "";
                GUI.FocusControl(null);
            }
        }


        private void OptionsBox ()
        {
            EditorGUIExtensions.DrawTitle ( "Options" );

            GUILayout.BeginHorizontal( EditorStyles.helpBox );

            if ( GUILayout.Button ( "Save Grid As..." ) )
            {

            }

            if ( GUILayout.Button ( "Reload" ) )
            {
                editorGroups = new Dictionary<Difficulty, List<EntityGroup>> ( grid.Groups );
            }

            GUILayout.EndHorizontal ();
        }

        private void AddGroup ( Difficulty difficulty, EntityGroup group )
        {
            group.id = group.GetHashCode ();

            if ( editorGroups.ContainsKey ( difficulty ) )
            {
                editorGroups [difficulty].Add ( group );
            }
            else
            {
                editorGroups.Add ( difficulty, new List < EntityGroup > () { group } );
            }

        }

        private void Load ()
        {
            var prefabs = Resources.LoadAll < GameObject > ( grid.prefabPath );

            List < EntityGroup > groups = PersistentManager.LoadFiles < EntityGroup > ( grid._cellDataPath );

            groups.AddRange ( prefabs.Select ( x => new EntityGroup () { id = x.GetInstanceID (), name = x.name , difficulty = Difficulty.Piece , prefab = x.name } ) );

            editorGroups = new Dictionary<Difficulty, List<EntityGroup>> ();

            if ( groups.Count > 0 )
            {
                foreach ( var group in groups )
                {
                    if ( editorGroups.ContainsKey ( group.difficulty ) )
                    {
                        editorGroups [ group.difficulty ].Add ( group.Clone () );
                    }
                    else
                    {
                        editorGroups.Add ( group.difficulty, new List < EntityGroup > () { group.Clone () });
                    }
                }
            }
        }

        private Texture2D MakeTex(Color col)
        {
            Color[] pix = new Color[1 * 1];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            Texture2D result = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            result.hideFlags = HideFlags.HideAndDontSave;
            result.SetPixels(pix);
            result.Apply();

            return result;
        }

	}
}
#endif