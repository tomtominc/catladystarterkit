﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;
using System.Linq;

#if ENABLE_GRIDS
using Gamelogic.Grids;

namespace Framework.Core
{
    public class GridBehaviour : GridBehaviour < RectPoint > , IWorldHandler
	{
        public IGrid < CellBehaviour, RectPoint > GridMap;

        [SerializeField]
        public List < GridColor > gridColors;

        public override void InitGrid()
        {
            GridMap = Grid.CastValues < CellBehaviour , RectPoint >();
            GridMap.Apply ( x => x.Initialize () );

            SetColors ( gridColors );
        }

        public void SaveAllGroups ()
        {
            Debug.Log ( "called on grid behaviour" );
        }

        public RectPoint VectorToRectPoint ( Vector2 position )
        {
            return Map [ position ];
        }

        public CellBehaviour GetCell ( Transform t )
        {
            foreach ( CellBehaviour cell in GridMap.Values )
            {
                if ( cell.Contains ( t ) ) return cell;
            }

            return null;
        }

        public CellBehaviour GetCell ( Vector2 position )
        {
            return GetCell(Map[position]);
        }

        public CellBehaviour GetCell (RectPoint point)
        {
            if ( ! GridMap.Contains(point) ) return null;

            return GridMap.GetCell(point);
        }

        public CellBehaviour GetCell < T > ()
        {
            foreach ( CellBehaviour cell in GridMap.Values)
            {
                if ( cell.Contains < T > () ) return cell;
            }

            return null;
        }

        public List < CellBehaviour > GetCells < T > ()
        {
            List < CellBehaviour > cells = new List < CellBehaviour > ();

            foreach ( CellBehaviour  cell in GridMap.Values )
            {
                if ( cell.Contains < T > () ) cells.Add ( cell );
            }

            return cells;
        }

        public void SetColors ( List < GridColor > colors )
        {
            foreach ( var gridColor in colors )
            {
                for ( int x = (int)gridColor.startPoint.x; x < (int)gridColor.endPoint.x; x++ )
                {
                    for ( int y = (int)gridColor.startPoint.y; y < (int)gridColor.endPoint.y; y++ )
                    {
                        CellBehaviour cell = GetCell ( new RectPoint ( x , y ) );
                        if ( cell != null ) cell.Color = gridColor.color;
                    } 
                }
            }
        }

        public bool FindInPath < T > ( RectPoint start, RectPoint direction )
        {
            RectPoint next = start;

            CellBehaviour current = GetCell ( next );

            while ( current != null)
            {
                next = next + direction;

                current = GetCell ( next );

                if ( current == null )
                    break;
                
                if ( current.Contains < T > () ) 
                {
                    return true;
                }
            }

            return false;
        }

        public IEntity Find < T > ()
        {
            CellBehaviour cell = GetCell < T > ();

            return cell.GetLayer < T > ();
        }
        public Vector2 DistanceFrom < T > ( Vector2 current )
        {
            RectPoint point = DistanceFrom < T > ( new RectPoint ( (int)current.x, (int)current.y ));
            return point.ToV2 ();
        }

        public RectPoint DistanceFrom < T > ( RectPoint current )
        {
            CellBehaviour cell = GetCell < T > ();

            if ( cell == null ) 
            {
                Debug.LogFormat ( "no cells contain {0}", typeof( T ) );
                return RectPoint.Zero;
            }

            RectPoint target = Map [ cell.transform.position ];

            return new RectPoint (  target.X - current.X , target.Y - current.Y );
        }

        public bool Move ( IEntity entity, bool instant, Vector2 current, Vector2 target, float duration, Action < Vector2 > onComplete )
        {
            // current cell can be null, this is usually the case when spawning an entity
            CellBehaviour currentCell = GetCell ( current );

            CellBehaviour targetCell = GetCell ( target );

            // taget cell can't be null
            if ( targetCell == null ) return false;

            Vector3 tPos = targetCell.WorldPosition + (Vector3)entity.OffsetPosition;

            if ( instant )
            {
                entity.Transform.position = tPos;

                if ( currentCell != null ) currentCell.Exit (entity);

                targetCell.Enter ( entity, target );
              
                if ( onComplete != null ) onComplete.Invoke ( target );

                return true;
            }

            Sequence sequence = DOTween.Sequence ();

            sequence.Append ( entity.Transform.DOMove ( tPos , duration ) );

            sequence.InsertCallback ( duration * 0.5f , 
                ()=>
                {
                    currentCell.Exit  ( entity  );
                    targetCell.Enter  ( entity , target  );
                });

            sequence.OnComplete ( () => { if ( onComplete != null ) onComplete.Invoke ( target ); } );

            sequence.Play ();

            return true;

        }

        public bool Despawn ( IEntity entity )
        {
            CellBehaviour cell = GetCell ( entity.Transform );

            if ( cell == null || entity == null ) return false;

            cell.Exit ( entity );

            GridManager.Despawn ( entity.Transform );

            return true;
        }

        public bool Spawn ( IEntity entity, Vector2 point )
        {
            CellBehaviour cell = GetCell ( point );

            if ( cell == null || entity == null ) return false;

            entity.Transform.position = cell.WorldPosition + (Vector3)entity.OffsetPosition;

            entity.Initialize ( this );

            return Move ( entity, true, Vector2.zero , point , 0f, null );

        }
	}
}

#endif