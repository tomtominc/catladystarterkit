﻿using UnityEngine;
using System.Collections;

namespace Framework.Core
{
    [System.Serializable]
    public class Item  
	{
        public string key;
        public int value;
	}
}