﻿using UnityEngine;
using System.Collections;
using Framework.Core.Animation;
using System.Collections.Generic;
using System.Linq;

#if ENABLE_GRIDS
using Gamelogic.Grids;

namespace Framework.Core
{
    public class CellBehaviour : SpriteCell 
	{
        [SerializeField]
        protected Transform _container;
        protected SpriteAnimation _animator;
        protected List < IEntity > _layers;
        protected Color _originalColor;

        public Vector3 WorldPosition 
        {
            get { return transform.position; }
        }

        public List < IEntity > Layers 
        {
            get  {  if ( _layers == null )  { _layers = new List < IEntity > (); } return _layers;  }
        }

        public void Initialize ()
        {
            _originalColor = Color;
        }

        public void ToOriginalColor ()
        {
            Color = _originalColor;
        }

        public List < IEntity > GetAllLayersOfType < T > ()
        {
            return _layers.FindAll ( x => x is T );
        }

        public IEntity GetLayer < T > ()
        {
            return _layers.Find ( x => x is T );
        }

        public bool Contains ( Transform t )
        {
            if ( _layers == null || _layers.Count <= 0 )
                return false;

            return _layers.Any ( x => x.Transform == t );
        }

        public bool Contains < T > ()
        {
            if ( _layers == null || _layers.Count <= 0 )
                return false;
            
            return _layers.Any ( x => x is T );
        }

        public void Enter ( IEntity entity , Vector2 point )
        {
            Layers.Add ( entity );
            entity.Transform.SetParent ( _container );
            entity.OnEnter ( point );

            DoEnterCollision ();
        }

        public void Exit ( IEntity entity )
        {
            if ( Layers.Contains ( entity ) )
            {
                DoExitCollision ();

                Layers.Remove ( entity );
                entity.OnExit ( );
            }
        }

        public void DoEnterCollision ()
        {
            List < IEntity > handlers = Layers.Where ( x => x is ICollisionHandler ).ToList ();
            List < IEntity > colliders = Layers.Where ( x => x is ICollider ).ToList ();

            colliders.ForEach ( x => handlers.ForEach ( y => ((ICollisionHandler)y).OnEnter ( ((ICollider)x))));
        }

        public void DoExitCollision ()
        {
            List < IEntity > handlers = Layers.Where ( x => x is ICollisionHandler ).ToList ();
            List < IEntity > colliders = Layers.Where ( x => x is ICollider ).ToList ();

            colliders.ForEach ( x => handlers.ForEach ( y => ((ICollisionHandler)y).OnExit ( ((ICollider)x))));
        }
	}
}
#endif