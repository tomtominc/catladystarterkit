﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using Framework.EditorTools;

namespace Framework.Core
{
    public class CharacterBehaviour : EntityBehaviour , IInputBehaviour, ICollider
    {
        [Header("Character Behaviour")]

        public LayerMask layer;

        public bool InverseControls { get { return false; } }

        protected bool handleInput = true;

        public LayerMask Layer
        {
            get { return layer; }
        }

        public override void Enabled()
        {
            EventManager.Publish(this, InputEvent.RegisterBehaviour, this, -1);
        }

        public override void Disabled()
        {
            EventManager.Publish(this, InputEvent.DeregisterBehaviour, this, 0);
        }

        public void HandleMobileInput(SwipeDirection swipe)
        {
            if (!handleInput)
                return;

            if (Move(swipe.ToVector2(), false, OnCompleteMoveAction))
            {
                handleInput = false;
            }

        }

        protected override void OnUpdate()
        {
//            primaryWeapon.Shoot (CurrentPoint.ToV2 ());
        }

        public void DoDamage(Damage damage)
        {
            EventManager.Publish(this, EntityEvent.OnDamage, damage);
        }

        public void DoPickup(Pickup pickup)
        {
            EventManager.Publish(this, EntityEvent.OnPickup, pickup);
        }

        private void OnCompleteMoveAction(Vector2 point)
        {
            transform.localPosition = OffsetPosition;
            handleInput = true;
        }

        public override void OnEnter(Vector2 point)
        {
            base.OnEnter(point);
        }
    }
}