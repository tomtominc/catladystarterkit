﻿using UnityEngine;
using System.Collections;
using Framework.Core.Animation;
using Framework.EditorTools;

namespace Framework.Core
{
    public class ProjectileBehaviour : EntityBehaviour, ICollisionHandler 
    {
        public enum State 
        {
            Move, Damage, Counter
        }

        [Header( "State Info")]
        public State state = State.Move;
        [MinMaxRange (0,10)]
        public Vector2 vulnerableFrames;

        [Header ( "Animation Options" ) ]
        public string prefix = string.Empty;

        [Header ( "Mover Options")]
        public bool teleport = false;
        public Vector2 direction = new Vector2 ( -1f, 0f );

        [Header ("Collision Options")]
        public LayerMask mask;
        public Damage damage;
        public Pickup counter;

        private float _moveDelay = 0f;
        private float _currentDelay = 0f;

        public LayerMask Mask 
        { 
            get { return mask; }
        }

        public void OnEnter ( ICollider collider )
        {
            if ( (Mask & collider.Layer ) != 0 )
            {
                if ( IsVulnerable () )
                {
                    state = State.Counter;
                    collider.DoPickup ( counter );
                }
                else
                {
                    state = State.Damage;
                    collider.DoDamage ( damage );
                }

                PlayAnimation ();
            }
        }

        public void OnExit ( ICollider collider ) { }

        public override void Enabled()
        {
            PlayAnimation ();
        }

        private bool MoveNext ()
        {
            return Move ( direction , teleport, OnMoveNext );
        }

        private void OnMoveNext ( Vector2 point )
        {
            PlayAnimation ();
        }

        private void PlayAnimation ()
        {
            Animator.PlayFormat ( prefix, state );
        }

        private bool IsVulnerable ()
        {
            return vulnerableFrames.InBetween (Animator.CurrentFrame);
        }

        private void Update ()
        {
            if ( Animator.CurrentFrameCount <= 0 ) return;

            if ( Animator.CurrentFrame >= ( Animator.CurrentFrameCount - 1 ) )
            {
                bool despawn = CompleteAction ( state );

                if ( despawn ) RemoveFromWorld ();
            }
        }

        private bool CompleteAction ( State state )
        {
            bool despawn = false;

            switch ( state )
            {
                case State.Move: despawn = !MoveNext (); break;
                case State.Damage: despawn = true; break;
                case State.Counter: despawn = true; break;
            }

            return despawn;
        }


        public override void RemoveFromWorld()
        {
            state = State.Move;
            base.RemoveFromWorld();
        }
    }
}