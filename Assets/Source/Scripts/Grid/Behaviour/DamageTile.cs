﻿using UnityEngine;
using System.Collections;

namespace Framework.Core
{
    public class DamageTile : EntityBehaviour, ICollisionHandler 
    {
        [SerializeField]
        public LayerMask mask;

        [SerializeField]
        public Damage damage;

        public LayerMask Mask 
        { 
            get { return mask; }
        }

        public void OnEnter ( ICollider collider )
        {
            
        }

        public void OnExit ( ICollider collider )
        {
            
        }
	}
}