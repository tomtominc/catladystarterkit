﻿using UnityEngine;
using System.Collections;

namespace Framework.Core
{
    [System.Serializable]
    public class EntityData
    {
        public string key;
        public Vector2 point;

        public EntityData Clone ()
        {
            return new EntityData { key = key , point = point };
        }
    }

}