﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Framework.Core
{
    public enum Difficulty
    {
        Start, Easy, Medium, Hard, Extreme, Piece
    }

    [System.Serializable]
    public class EntityGroup : ISelectable
    {   
        
        public int id = 0;
        public string name = string.Empty;
        public string prefab = string.Empty;

        public Difficulty difficulty;

        public float selectionWeight = 1f;

        public float GetSelectionWeight()
        {
            return selectionWeight;
        }

        public float AdjustedSelectionWeight 
        { 
            get { return selectionWeight;  }
            set { selectionWeight = value; }
        }

        public EntityGroup Clone ()
        {
            return new EntityGroup () 
            { 
                id = id,
                name = name,
                selectionWeight = selectionWeight,
                difficulty = difficulty,
                prefab = prefab
            };
        }
    }
}