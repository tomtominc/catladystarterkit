﻿using UnityEngine;
using System.Collections;

namespace Framework.Core
{
    [System.Serializable ]
    public class Damage
    {
        public enum Type
        {
            None, Spike, Fire, Beam
        }

        public int value;
        public Type type;
    }

    [System.Serializable ]
    public class Pickup
    {
        public enum Type
        {
            None, Coin, Counter, Star, Chain
        }

        public int value;
        public Type type;
    }

    public interface ICollider 
	{
        LayerMask Layer { get; }

        void DoDamage ( Damage damage );
        void DoPickup ( Pickup pickup );
	}

    public interface ICollisionHandler 
    {
        LayerMask Mask { get; }

        void OnEnter ( ICollider collider );
        void OnExit  ( ICollider collider );
    }

    public interface IDamagable : ICollider
    {
        int Health { get; }
        void Damage ( Damage damage );
    }
}