﻿using UnityEngine;
using System.Collections;
using Framework.Core.Animation;
using System;

namespace Framework.Core
{
    public interface IWorldHandler 
    {
        bool Spawn ( IEntity entity, Vector2 point );
        bool Despawn ( IEntity entity );
        bool Move ( IEntity entity, bool instant, Vector2 current, Vector2 target, float duration, Action < Vector2 > onComplete );

        IEntity Find < T > ();
    }

    public interface IEntity
    {
        string Name { get; }

        Transform Transform { get; }

        SpriteRenderer SpriteRenderer { get; }

        SpriteAnimation Animator { get; }

        Vector2 CurrentPoint { get; }

        Vector2 OffsetPosition { get; }

        Color OccupiedColor { get; }

        void Initialize ( IWorldHandler world );

        void OnEnter ( Vector2 point );
        void OnExit  ();
    }

   
}