﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if ENABLE_GRIDS
using Gamelogic.Grids;


namespace Framework.Core
{
	public static class GridExtension 
	{
        public static Dictionary < SwipeDirection , RectPoint > SwipeToDirectionMap 
        = new Dictionary<SwipeDirection, RectPoint>()
        {
            { SwipeDirection.Right , RectPoint.East },
            { SwipeDirection.Left  , RectPoint.West },
            { SwipeDirection.Up    , RectPoint.North },
            { SwipeDirection.Down  , RectPoint.South }
        };

        public static CellBehaviour GetNeighbour ( GridBehaviour grid , RectPoint current , RectPoint direction )
        {
            return grid.GetCell ( current + direction );
        }
	}
}

#endif