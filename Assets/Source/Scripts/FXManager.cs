﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using Framework.Utilities;

namespace Framework.Core
{
    public class FXManager : Singleton<FXManager>
    {
        private SpawnPool _pool;

        private void Start()
        {
            _pool = GetComponent < SpawnPool >();
        }

        public static Transform Spawn(string effect, Vector3 position)
        {
            return Instance._pool.Spawn(effect, position, Quaternion.identity);
        }

        public static void Despawn(Transform entity)
        {
            Instance._pool.Despawn(entity);
        }

        public static void Spawn(string effect, Vector3 position, float despawnInTime)
        {
            Transform entity = Instance._pool.Spawn(effect, position, Quaternion.identity);
            Instance.StartCoroutine(Instance.Despawn(entity, despawnInTime));
        }

        private IEnumerator Despawn(Transform entity, float delay)
        {
            yield return new WaitForSeconds(delay);
            _pool.Despawn(entity);
        }

    }
}