﻿using UnityEngine;
using System.Collections;
using System;

namespace Framework.Core
{
	public class TimeSystem : MonoBehaviour 
	{
        public static DateTime GetTimeFromNow ( TimeSpan span )
        {
            return DateTime.UtcNow.Add ( span );
        }
	}
}