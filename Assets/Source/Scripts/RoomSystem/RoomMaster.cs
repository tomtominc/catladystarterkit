﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Framework.Core;
using Framework.Core.EventSystem;
using PathologicalGames;
using UnityEngine.UI;
using RutCreate.LightningDatabase;

public class RoomLoadData
{
    public List < PersistentData > persistableData;
}

public enum Location
{
    House,
    Party,
    Scifi
}


public enum RoomEvent
{
    OnSwitchRoom,
    OnSwitchedRoom,
    OnAddToRoom,
    OnRemoveFromRoom,
    OnCatAddedToQueue
}

public class RoomMaster : Loader
{
    public static RoomMaster Instance;

    [HideInInspector]public Location CurrentLocation = Location.House;
    [HideInInspector]public Dictionary < Location , Room > Rooms;
    private SpawnPool _pool;

    [SerializeField]private string _productionFormat = "{0} Coins/Sec";
    [SerializeField]private Text _productionLabel;


    private int _totalCoinsPerSecond;
    private float _currentTime = 0f;

    public Room CurrentRoom { get { return Rooms[CurrentLocation]; } }

    public static int HouseEntityCount
    { 
        get
        { 
            if (Instance.Rooms == null)
                return 0;

            if (Instance.Rooms[Location.House].PersistentData == null)
                return 0;

            if (Instance.CurrentLocation == Location.House)
                return Instance.Rooms[Location.House].PersistentData.Count + Instance.Rooms[Location.House].QueuedData.Count; 
            
            return Instance.Rooms[Location.House].LoadedPersistentData.Count + Instance.Rooms[Location.House].QueuedData.Count; 
        } 
    }

    private void Awake()
    {
        Instance = this;
    }

    public override IEnumerator Load()
    {
        PersistentManager.Save("RoomType", Location.House);

        _pool = GetComponent < SpawnPool >();
        Rooms = new Dictionary<Location, Room>();

        foreach (Transform child in transform)
        {
            Room room = child.GetComponent < Room >();
            Rooms.Add(room.roomType, room);
        }
           
        EventManager.Subscribe(RoomEvent.OnSwitchRoom, OnSwitchRoom);
        EventManager.Subscribe(RoomEvent.OnAddToRoom, OnAddToRoom);
        EventManager.Subscribe(RoomEvent.OnRemoveFromRoom, OnRemoveFromRoom);
        EventManager.Subscribe(GameStateEvent.OnChangeGameState, OnLoadGameState);

        yield break;
    }

    private void OnLoadGameState(IMessage message)
    {
        GameState state = (GameState)message.Data;

        if (state == GameState.NewGame)
        {
            ResetData();
        }

        LoadSettings();
    }


    private void LoadSettings()
    {
        Location lastRoom = PersistentManager.Load("RoomType", Location.House);

        EventManager.Publish(this, RoomEvent.OnSwitchRoom, lastRoom);
    }

    public void Save()
    {
        CurrentRoom.Save();

        foreach (var room in Rooms)
        {
            if (room.Key != CurrentLocation)
                room.Value.SaveQueue();
        }
    }

    private void ResetData()
    {
        foreach (var room in Rooms)
        {
            room.Value.ResetData();
        }

        PersistentManager.Save("RoomType", Location.House);
        PersistentManager.Save(Location.House.ToString() + "_Unlocked", false);
        PersistentManager.Save(Location.Party.ToString() + "_Unlocked", false);
        PersistentManager.Save(Location.Scifi.ToString() + "_Unlocked", false);
    }

    private void OnSwitchRoom(IMessage message)
    {
        Location _lastRoom = CurrentLocation;
        CurrentLocation = (Location)message.Data;

        if (_lastRoom != CurrentLocation)
        {
            Rooms[_lastRoom].Save();
            Rooms[_lastRoom].Unload();
            _pool.Despawn(Rooms[_lastRoom].transform);
        }

        if (CurrentRoom.gameObject.activeSelf && _lastRoom == CurrentLocation)
            return;
        
        _pool.Spawn(CurrentRoom.roomType.ToString()).GetComponent < Room >();

        CurrentRoom.Initialize();
        CurrentRoom.Load();

        Hashtable table = new Hashtable();
        table.Add("LastRoom", _lastRoom);
        table.Add("CurrentRoom", CurrentLocation);

        EventManager.Publish(this, RoomEvent.OnSwitchedRoom, table);

    }

    public void OnAddToRoom(IMessage message)
    {
        PersistentData data = (PersistentData)message.Data;
        Location roomType = Enum < Location >.Parse(data.PoolName);

        bool hasBeenUnlocked = PersistentManager.Load(roomType.ToString() + "_Unlocked", false);

        if (!hasBeenUnlocked)
            PersistentManager.Save(roomType.ToString() + "_Unlocked", true);

        if (roomType == CurrentLocation)
        {
            CurrentRoom.Spawn(data);
        }
        else
        {
            Rooms[roomType].AddToQueue(data);

            Hashtable table = new Hashtable();
            table.Add("HasBeenUnlocked", hasBeenUnlocked);
            table.Add("CurrentType", CurrentLocation);
            table.Add("RoomType", roomType);
            table.Add("TotalInQueue", Rooms[roomType].QueuedData.Count);

            EventManager.Publish(this, RoomEvent.OnCatAddedToQueue, table);
        }
    }

    public void OnRemoveFromRoom(IMessage message)
    {
        Transform entity = (Transform)message.Data;

        CurrentRoom.Despawn(entity);
    }

    private void Update()
    {
        if (Rooms == null)
            return;

        if (_currentTime + 1f > Time.time)
            return;

        _currentTime = Time.time;

        float amount = 0f;

        foreach (var room in Rooms)
        {
            if (room.Key == CurrentLocation)
            {
                amount += room.Value.GetProductionValue();
            }
            else
            {
                amount += room.Value.GetLoadedProductionValue();
            }
               
        }

        _productionLabel.text = string.Format(_productionFormat, amount.ToString("N2"));
    }

   

    public void ButtonClick_SetRoom(int step)
    {
        EventManager.Publish(this, RoomEvent.OnSwitchRoom, CurrentLocation.Move(step));
    }

    private void OnDisable()
    {
        Save();
    }
}
