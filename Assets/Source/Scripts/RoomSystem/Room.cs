﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Framework.Core;
using PathologicalGames;
using System.Linq;
using Framework.Core.EventSystem;
using System.Xml.Linq;
using RutCreate.LightningDatabase;

public class Room : MonoBehaviour
{
    public Location roomType;

    private SpawnPool _pool;

    private List < PersistentData > _persistentData;
   
    private List < Transform > _entities;

    private List < PersistentData > _loadedData;

    public List < PersistentData > PersistentData
    {
        get { return _persistentData; }
    }

    public List < Transform > Entities
    {
        get { return _entities; }
    }

    public List < PersistentData > LoadedPersistentData
    {
        get { return PersistentManager.Load(roomType.ToString(), new List<PersistentData>()); }
        set { PersistentManager.Save(roomType.ToString(), value); }
    }

    private List < PersistentData > _queuedData;

    public List < PersistentData > QueuedData
    {
        get { return _queuedData; }
    }

    public void Awake()
    {
        _queuedData = PersistentManager.Load(roomType.ToString() + "_Queue", new List < PersistentData >());
    }

    public void Initialize()
    {
        _pool = GetComponent < SpawnPool >();
        _persistentData = new List<PersistentData>();
        _entities = new List<Transform>();

        if (_loadedData != null)
        {
            _loadedData.Clear();
            _loadedData = null;
        }
       
    }

    public void Save()
    {
        PersistentManager.Save(roomType.ToString(), ReloadEntities());

    }

    public void SaveQueue()
    {
        PersistentManager.Save(roomType.ToString() + "_Queue", _queuedData);
    }

    public void Load()
    {
        LoadedPersistentData.ForEach(x => Spawn(x));

        if (_queuedData != null)
        {
            _queuedData.ForEach(x => Spawn(x));
            _queuedData.Clear();
        }
       
    }

    public void ResetData()
    {
        PersistentManager.Save(roomType.ToString(), new List < PersistentData >());
    }

    public void Unload()
    {
        if (_pool == null)
            return;
        
        _pool.DespawnAll();
        _entities.Clear();
        _persistentData.Clear();
    }

    public List < PersistentData > ReloadEntities()
    {
        if (_entities == null)
            return LoadedPersistentData;
        
        _persistentData = new List<Framework.Core.PersistentData>();
        _entities.ForEach(entity => ReloadData(entity));

        return _persistentData;
    }

    public void ReloadData(Transform entity)
    {
        var data = entity.GetComponent < IPersistent < PersistentData > >().Data;

        _persistentData.Add(data);
    }

    public void AddToQueue(PersistentData data)
    {
        _queuedData.Add(data);
    }

    public float GetLoadedProductionValue()
    {
        if (_persistentData == null || _queuedData == null)
            return 0f;
        
        if (_loadedData == null)
        {
            _loadedData = new List<PersistentData>();
            _loadedData.AddRange(LoadedPersistentData);
            _loadedData.AddRange(QueuedData);
        }

        float amount = 0f;

        foreach (var data in _loadedData)
        {
            CatBlueprint blueprint = CatDex.GetBlueprint(data.PrefabName);

            if (blueprint == null)
                continue;

            amount += blueprint.ProductionPerSecond;
        }

        return amount;
    }

    public float GetProductionValue()
    {
        float amount = 0f;

        if (_persistentData == null)
            return 0f;

        for (int i = 0; i < _persistentData.Count; i++)
        {
            PersistentData data = _persistentData[i];
            CatBlueprint blueprint = CatDex.GetBlueprint(data.PrefabName);

            if (blueprint == null)
                continue;

            amount += blueprint.ProductionPerSecond;

            ITimedEntity timedEntity = _entities[i].GetComponent < ITimedEntity >();
            timedEntity.Tick();
        }

        return amount;

    }

    public void Spawn(PersistentData data)
    {
        Transform entity = data.Load();

        _entities.Add(entity);
        _persistentData.Add(data);
    }

    public void Despawn(Transform entity)
    {
        if (!entity.gameObject.activeSelf)
            return;
        
        var data = _persistentData.FirstOrDefault(x => x.InstanceID == entity.GetInstanceID());
    
        _persistentData.Remove(data);
        _entities.Remove(entity);
    
        _pool.Despawn(entity);
    }


}
