﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using Framework.Core.EventSystem;
using DoozyUI;
using System.Collections.Generic;

public class RoomArrowUI : Loader
{
    public RoomMaster roomMaster;
    [SerializeField]private UIElement _rightRoomArrow;
    [SerializeField]private UIElement _leftRoomArrow;
    [SerializeField]private UIElement _rightRoomNotification;
    [SerializeField]private UIElement _leftRoomNotification;

    public override IEnumerator Load()
    {
        EventManager.Subscribe(RoomEvent.OnSwitchedRoom, OnSwitchedRoom);
        EventManager.Subscribe(RoomEvent.OnCatAddedToQueue, OnCatAddedToQueue);
        yield break;
    }

    private void OnSwitchedRoom(IMessage message)
    {
        Hashtable table = (Hashtable)message.Data;

        Location currentRoom = (Location)table["CurrentRoom"];
        List < string > visibleElements = UIManager.GetVisibleUIElementElementNames();

        if (currentRoom.IsFirst())
        {
            _leftRoomArrow.Hide(false);
        }
        else if (!_leftRoomArrow.isVisible)
        {
            _leftRoomArrow.gameObject.SetActive(true);
            _leftRoomArrow.Show(false);
           
        }
       

        if (currentRoom.IsLast())
        {
            _rightRoomArrow.Hide(false);
        }
        else if (PersistentManager.Load(currentRoom.Next().ToString() + "_Unlocked", false) && !_rightRoomArrow.isVisible)
        {
            _rightRoomArrow.gameObject.SetActive(true);
            _rightRoomArrow.Show(false);
        }
        else
        {
            _rightRoomArrow.Hide(false);
        }
            
        if (_leftRoomNotification.isVisible)
            _leftRoomNotification.Hide(false);

        if (_rightRoomNotification.isVisible)
            _rightRoomNotification.Hide(false);

    }

    private void OnCatAddedToQueue(IMessage message)
    {
        Hashtable table = (Hashtable)message.Data;

        Location currentType = (Location)table["CurrentType"];
        Location type = (Location)table["RoomType"];
        int totalInQueue = (int)table["TotalInQueue"];

        bool rightRoom = (currentType.ToInt() - type.ToInt()) < 0;

        UIElement arrow = rightRoom ? _rightRoomArrow : _leftRoomArrow;
        UIElement notification = rightRoom ? _rightRoomNotification : _leftRoomNotification;

        if (!arrow.isVisible)
        {
            arrow.gameObject.SetActive(true);
            arrow.Show(false);
        }
           

        if (!notification.isVisible)
        {
            notification.gameObject.SetActive(true);
            notification.Show(false);
        }
           

        notification.GetComponent < NotificationUI >().SetNotifications(totalInQueue);

    }
}
