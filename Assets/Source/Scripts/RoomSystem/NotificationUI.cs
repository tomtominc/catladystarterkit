﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotificationUI : MonoBehaviour
{
    [SerializeField] private Text _text;

    public void SetNotifications(int count)
    {
        _text.text = count.ToString();
    }
}
