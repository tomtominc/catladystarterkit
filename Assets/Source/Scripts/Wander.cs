﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Framework.Core
{
	public class Wander : MonoBehaviour 
	{
        public Rect wanderBounds;
        public LayerMask groundLayer;
        public Vector3 defaultPoint;

        public Vector2 _wanderDelay = new Vector2 (1f, 10f);
        public float _turnSpeed = 2f;
        public float _speed = 0.5f;
        public Vector3 _currentPoint;

        private Sequence _currentSequence;

        private void Start ()
        {
            DoSequence ();
        }

        public void OnTouched ()
        {
            if ( _currentSequence != null )
            {
                _currentSequence.Kill ( false );
            }
        }

        public void OnReleased ()
        {
            DoSequence ();
        }

        private void DoSequence ()
        {
            _currentSequence = DOTween.Sequence ();

            _currentSequence.Append ( RotateToDirection ());
            _currentSequence.Append ( MoveToPoint () );
            _currentSequence.OnComplete ( DoSequence );
            _currentSequence.Play ();
        }

        private Tween RotateToDirection ()
        {
            _currentPoint = GetWanderPoint ();

            _currentPoint.y = 1f;

            Vector3 _direction = (_currentPoint - transform.position ).normalized;

            Quaternion _lookRotation = Quaternion.LookRotation ( _direction );

            Vector3 _lookDirection = _lookRotation.eulerAngles;

            _lookDirection.y += 90f;
            _lookDirection.z = 0f;
            _lookDirection.x = 0f;

            return transform.DORotate ( _lookDirection , _turnSpeed );
        }

        private Tween MoveToPoint ()
        {
            return transform.DOMove ( _currentPoint, _speed ).SetSpeedBased ( true );
        }

        private Vector3 GetWanderPoint ()
        {
            bool found = false;
            int tryCount = 0;

            while (!found)
            {

                Vector3 origin = new Vector3 ( Random.Range ( wanderBounds.x, wanderBounds.width), 10f , Random.Range ( wanderBounds.y, wanderBounds.height ) );

                RaycastHit hit;

                if ( Physics.Raycast ( origin, Vector3.down , out hit , Mathf.Infinity ) )
                {
                    if ( hit.transform != null && ((1 << hit.transform.gameObject.layer) & groundLayer) != 0 )
                    {
                        found = true;
                        return hit.point;
                    }
                }

                tryCount++;

                if ( tryCount >= 20 )
                {
                    found = true;
                    return defaultPoint;
                }
            }

            return defaultPoint;
        }
	}
}