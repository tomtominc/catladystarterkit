﻿using UnityEngine;
using System.Collections;
using RutCreate.LightningDatabase;

namespace Framework.Core
{
    public interface IUpgradable
    {
        void LoadUpgrade(UpgradeBlueprint defaultUpgrade);

        void SetUpgrade(UpgradeBlueprint upgrade);

        void Upgrade();

        UpgradeBlueprint Current { get; }
    }
}