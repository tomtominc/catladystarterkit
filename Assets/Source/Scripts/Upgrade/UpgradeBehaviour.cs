
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;
using Framework.Core.EventSystem;
using RutCreate.LightningDatabase;

namespace Framework.Core
{
    public class UpgradeBehaviour : MonoBehaviour , IUpgradable
    {
        private UpgradeBlueprint _upgrade;

        [Header("GUI Components")]
        public Text title;
        public Text description;
        public Text upgradeValue;

        public Text upgradeCost;
        public Text upgradePremiumCost;

        public Transform iconParent;

        public UpgradeBlueprint Current { get { return _upgrade; } }

        public virtual void LoadUpgrade(UpgradeBlueprint defaultUpgrade)
        {
            EventManager.Subscribe(GameStateEvent.OnChangeGameState, OnLoadGameState);

            SetUpgrade(defaultUpgrade);

            var icon = Instantiate < GameObject >(defaultUpgrade.Icon);

            icon.transform.SetParent(iconParent, false);

            icon.transform.localScale = new Vector3(2f, 2f, 2f);

            icon.transform.localPosition = new Vector3(0f, -33f, 0f);
        }

        private void OnLoadGameState(IMessage message)
        {
            GameState state = (GameState)message.Data;

            if (state == GameState.NewGame)
            {
                _upgrade.CurrentUpgrade = _upgrade.MinUpgrade;

                SetUpgrade(_upgrade);
            }
        }

        public virtual void SetUpgrade(UpgradeBlueprint upgrade)
        {
            _upgrade = upgrade;

            title.text = _upgrade.Name;
            description.text = _upgrade.Description;
            upgradeValue.text = GetUpgradeValue();
            upgradeCost.text = GetUpgradeCostFormat();
            upgradePremiumCost.text = _upgrade.BasePremiumCost.ToString();

            EventManager.Publish(this, UpgradeEvent.OnUpgrade, this);
        }

        public virtual void Upgrade()
        {
            if (GetUpgradeCost() <= StatsManager.Instance.Gold)
            {
                EventManager.Publish(this, ScoreEvent.Gold, -GetUpgradeCost());

                _upgrade.CurrentUpgrade = (int)GetNextUpgrade();

                SetUpgrade(_upgrade);
            }
        }

        public virtual bool IsMaxUpgrade()
        {
            return Mathf.Abs(_upgrade.MinUpgrade - _upgrade.CurrentUpgrade) >= Mathf.Abs(_upgrade.MinUpgrade - _upgrade.MaxUpgrade);
        }

        public virtual string GetUpgradeValue()
        {
            if (IsMaxUpgrade())
                return string.Format("MAX ({0})", _upgrade.MaxUpgrade);
            
            return string.Format("{0}{1} -> {2}{3}", _upgrade.CurrentUpgrade, _upgrade.Suffix, GetNextUpgrade(), _upgrade.Suffix);
        }

        public virtual float GetNextUpgrade()
        {
            return _upgrade.CurrentUpgrade + (_upgrade.Step * Mathf.Sign(_upgrade.MaxUpgrade - _upgrade.MinUpgrade));
        }

        public virtual float GetUpgradeCost()
        {
            return _upgrade.BaseCost * Mathf.Pow(5.5f, Mathf.Abs(_upgrade.MinUpgrade - _upgrade.CurrentUpgrade));
        }

        public virtual string GetUpgradeCostFormat()
        {
            if (IsMaxUpgrade())
                return string.Format("MAX");
            
            return GetUpgradeCost().ToString("N2");
        }
    }
}