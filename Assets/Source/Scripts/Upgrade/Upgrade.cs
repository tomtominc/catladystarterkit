﻿using UnityEngine;
using System.Collections;
using Framework.EditorTools;

namespace Framework.Core
{
    public enum UpgradeType
    {
        Quality, Delivery
    }

    [System.Serializable]
    public class Upgrade
	{
        public void Initialize ()
        {
            current = min;
            direction = Mathf.Sign ( max - min );
        }
       
        public string name;
        public UpgradeType type;
        [TextArea]
        public string description;
        public float baseCost;
        public float premiumCost;
        public string suffix;
        public string icon;


        public float min;
        public float max;
        public float direction { get; set; } 
        public float step;
        public float current { get; set; }
	}
}