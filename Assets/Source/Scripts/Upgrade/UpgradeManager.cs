﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Framework.Core.EventSystem;
using RutCreate.LightningDatabase;

namespace Framework.Core
{
    public enum UpgradeEvent
    {
        OnUpgrade,
        OnUpgradeQuality,
        OnUpgradeDelivery
    }

    public class UpgradeManager : Loader
    {
        public GameObject upgradePrefab;

        [SerializeField]private UpgradeBlueprintDatabase _upgradeBlueprintDatabase;

        [Header("Upgrade Items")]
        public Material CrateMaterial;
        public List < Texture > CrateTextures;

        public override IEnumerator Load()
        {
            yield return new WaitForEndOfFrame();

            EventManager.Subscribe(UpgradeEvent.OnUpgrade, OnUpgrade);

            foreach (UpgradeBlueprint upgrade in _upgradeBlueprintDatabase.FindAll())
            {
                GameObject prefab = Instantiate < GameObject >(upgradePrefab);
                prefab.transform.SetParent(transform, false);

                IUpgradable upgradable = prefab.GetComponent < IUpgradable >();
                upgradable.LoadUpgrade(upgrade);
            }
        }

        public void OnDisable()
        {
            EventManager.Unsubscribe(UpgradeEvent.OnUpgrade, OnUpgrade);
        }

        public void OnUpgrade(IMessage message)
        {
            IUpgradable upgrade = (IUpgradable)message.Data;

            if (upgrade.Current.Name.Equals("CrateQuality"))
            {
                UpgradeQuality(upgrade);
            }
            else if (upgrade.Current.Name.Equals("CatDelivery"))
            {
                UpgradeDelivery(upgrade);
            }
        }

        public void UpgradeDelivery(IUpgradable upgrade)
        {
            EventManager.Publish(this, UpgradeEvent.OnUpgradeDelivery, upgrade);
        }

        public void UpgradeQuality(IUpgradable upgrade)
        {
            EventManager.Publish(this, UpgradeEvent.OnUpgradeQuality, upgrade);

            int index = (int)(upgrade.Current.CurrentUpgrade - 1);

            Debug.LogFormat("Index: {0}", index);
            if (index >= CrateTextures.Count)
                return;

            CrateMaterial.mainTexture = CrateTextures[index];
        }
    }
}