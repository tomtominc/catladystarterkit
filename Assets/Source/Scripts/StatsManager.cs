﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;

namespace Framework.Core
{
	public class StatsManager : Loader 
	{
        public static StatsManager Instance;

        private float _gold;

        public override IEnumerator Load()
        {
            EventManager.Subscribe ( GameStateEvent.OnChangeGameState , OnLoadGameState );

            Gold = PersistentManager.Load ( "Gold_Value", 0f );

            yield return new WaitForEndOfFrame ();
        }

        public float Gold
        {
            get { return _gold; }
            set { _gold = value; }
        }

        private void Awake ()
        {
            Instance = this;
        }

        private void OnDisable ()
        {
            Save ();
        }

        private void Save ()
        {
            PersistentManager.Save ( "Gold_Value", _gold);
        }

        private void OnLoadGameState ( IMessage message )
        {
            GameState state = (GameState)message.Data;

            if ( state == GameState.NewGame )
            {
                _gold = 0;

                Save ();
            }
        }

	}
}