﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using Newtonsoft.Json;
using HardCodeLab.TutorialMaster;

namespace Framework.Core
{
    public class TimeManager : Loader
    {
        private System.DateTime _realTime { get { return System.DateTime.Now; } }

        public int maxCats = 8;

        public Rect crateSpawnBounds;
        public LayerMask crateValidSpawnLayers;
        public float crateProductionInSeconds = 10f;
        public string cratePrefab = "Crate";
        public List <AudioClip> spawnSounds;
        public GameObject spawnParticle;

        public Slider crateSpawnBar;

        private float _timeForCrate = 0f;

        private float _time = 0f;

        public Text goodsLabel;

        private bool _canProduceGoods = false;

        private float GoldAmount
        {
            get { return StatsManager.Instance.Gold; }
            set { StatsManager.Instance.Gold = value; }
        }

        public override IEnumerator Load()
        {
            _time = Time.time;
            _timeForCrate = Time.time;

            EventManager.Subscribe(GameStateEvent.OnChangeGameState, OnLoadGameState);
            EventManager.Subscribe(UpgradeEvent.OnUpgradeDelivery, OnUpdateDelivery);
            EventManager.Subscribe(ScoreEvent.Gold, UpdateGold);


            LoadData();

            yield break;
        }

        private void OnDisable()
        {
            EventManager.Unsubscribe(GameStateEvent.OnChangeGameState, OnLoadGameState);
            EventManager.Unsubscribe(UpgradeEvent.OnUpgradeDelivery, OnUpdateDelivery);
            EventManager.Unsubscribe(ScoreEvent.Gold, UpdateGold);
        }

        private void OnLoadGameState(IMessage message)
        {
            GameState state = (GameState)message.Data;

            if (state == GameState.NewGame)
            {
                _canProduceGoods = false;
            }
            else
            {
                _canProduceGoods = true;
            }
        }

        private void Update()
        {
            if (!_canProduceGoods)
                return;

            crateSpawnBar.value = (Time.time - _timeForCrate) / crateProductionInSeconds;

            if (Time.time >= _timeForCrate + crateProductionInSeconds && RoomMaster.HouseEntityCount < maxCats)
            {
                SpawnCrate();

                _timeForCrate = Time.time;
            }
        }

        public void LoadData()
        {
            goodsLabel.text = GoldAmount.ToString("N2");
        }

        private void OnUpdateDelivery(IMessage message)
        {
            IUpgradable upgrade = (IUpgradable)message.Data;
            crateProductionInSeconds = upgrade.Current.CurrentUpgrade;
        }

        public void SpawnCrate()
        {
            bool spawned = false;
            int tryCount = 0;

            while (!spawned)
            {

                Vector3 origin = new Vector3(Random.Range(crateSpawnBounds.x, crateSpawnBounds.width), 10f, Random.Range(crateSpawnBounds.y, crateSpawnBounds.height));

                RaycastHit hit;

                if (Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity))
                {
                    if (hit.transform != null && ((1 << hit.transform.gameObject.layer) & crateValidSpawnLayers) != 0)
                    {
//                        var p = Instantiate < GameObject >(spawnParticle);
//
//                        p.transform.position = hit.point;

                        PersistentData spawnData = new PersistentData(cratePrefab, Location.House, hit.point);
                        EventManager.Publish(this, RoomEvent.OnAddToRoom, spawnData);

//                        foreach (var spawnSound in spawnSounds)
//                            AudioCenter.PlayClip(spawnSound, 1f);

                        spawned = true;

                        break;
                    }
                }

                tryCount++;

                if (tryCount >= 10)
                {
                    spawned = true;
                    break;
                }
            }
        }

       
        public void UpdateGold(IMessage message)
        {
            float score = (float)message.Data;

            GoldAmount += score;

            goodsLabel.text = GoldAmount.ToString("N2");

        }

        public void OnFrameEnter()
        {
            if (tutorial.id == 0 && frame.id == 5)
            {
                SpawnCrate();
            }

            if (tutorial.id == 0 && frame.id == 8)
            {
                _canProduceGoods = true;
                _timeForCrate = Time.time;
            }
        }
    }
}