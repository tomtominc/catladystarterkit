﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RutCreate.LightningDatabase;

namespace Framework.Core
{
    public class CatDexGUI : Loader
    {
        [ Header("Settings")]
        public CatDexIcon icon;

        [Header("GUI Elements")]
        public Text Name;
        public Text Description;
        public Text CoinsPerSecond;

        [Header("Button Elements")]
        public GameObject nextButton;
        public GameObject previousButton;

        private CatBlueprint current;

        public override IEnumerator Load()
        {

            icon.Init();

            Set(CatDex.GetBlueprint(1));

            yield break;
        }

        public void Next()
        {
            Set(CatDex.GetBlueprint(current.ID + 1));
        }

        public void Previous()
        {
            Set(CatDex.GetBlueprint(current.ID - 1));
        }

        private void Set(CatBlueprint data)
        {
            if (data == null)
                return;
            
            current = data;

            Name.text = current.Name;
            Description.text = current.Description;
            CoinsPerSecond.text = current.ProductionPerSecond.ToString("N2");

            icon.Set(current);

            previousButton.SetActive(current.ID > 1);
            nextButton.SetActive(current.ID < CatDex.Count());

        }


    }
}