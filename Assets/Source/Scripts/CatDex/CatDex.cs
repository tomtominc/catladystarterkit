﻿using UnityEngine;
using RutCreate.LightningDatabase;
using System.Collections;
using System.Collections.Generic;

namespace Framework.Core
{
    public interface ITimedEntity
    {
        float ProductionPerSecond { get; }

        void Tick();

    }

    public class CatDex  : Loader
    {
        public static CatDex Instance;

        [SerializeField] private CatBlueprintDatabase _catDatabase;

        public override IEnumerator Load()
        {
            Instance = this;

            yield return null;
        }

        public static CatBlueprint GetBlueprint(string cat)
        {
            return Instance._catDatabase.Find(cat);
        }

        public static CatBlueprint GetBlueprint(int id)
        {
            Debug.Log(id); 
            return Instance._catDatabase.Find(id);
        }

        public static string GetName(int id)
        {
            return Instance._catDatabase.Find(id).Name;
        }

        public static bool Contains(string name)
        {
            return Instance._catDatabase.Find(name) == null;
        }

        public static int Count()
        {
            return Instance._catDatabase.Count();
        }

        public static bool HasBeenDiscovered(string name)
        {
            CatBlueprint blueprint = Instance._catDatabase.Find(name);

            if (blueprint == null)
                return true;
            
            return blueprint.Unlocked;
        }

        public static void SetDiscovered(string name, bool value)
        {
            CatBlueprint blueprint = Instance._catDatabase.Find(name);

            if (blueprint != null)
                blueprint.Unlocked = value;
        }

        public static void ResetCatSavedData()
        {
            foreach (CatBlueprint blueprint in Instance._catDatabase.FindAll())
            {
                blueprint.Unlocked = false;
                blueprint.Purchased = 0;
            }
        }

        public static List < CatBlueprint > GetAllCats()
        {
            return Instance._catDatabase.FindAll();
        }
    }
}