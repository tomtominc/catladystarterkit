﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RutCreate.LightningDatabase;

namespace Framework.Core
{
    public class CatDexIcon : MonoBehaviour
    {
        [SerializeField]public Transform _dummyCatParent;

        private Dictionary < string , Transform > icons;

        [SerializeField]private Vector3 _catOffset;
        [SerializeField]private Vector3 _catScale;
        [SerializeField]private Vector3 _catRotation = new Vector3(0f, 180f, 0f);

        public void Init()
        {
            if (_dummyCatParent == null)
                _dummyCatParent = GameObject.Find("DummyCats").transform;
            
            icons = new Dictionary<string, Transform >();

            foreach (Transform child in _dummyCatParent)
            {
                string gen = child.name;

                if (icons.ContainsKey(gen))
                    continue;

                icons.Add(gen, child);
            }
        }

        public void Set(CatBlueprint data)
        {
            if (transform.childCount > 0)
                transform.DestroyChildren();

            if (!icons.ContainsKey(data.Name))
                return;

            Transform icon = Instantiate < Transform >(icons[data.Name]);

            icon.SetParent(transform, false);

            icon.localPosition = _catOffset;
            icon.localScale = _catScale;
            icon.eulerAngles = _catRotation;

            icon.gameObject.SetActive(true);

        }


    }
}