﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using PathologicalGames;
using RutCreate.LightningDatabase;
using UnityEngine.UI;

namespace Framework.Core
{
    public enum ScoreEvent
    {
        Gold,
    }

    public enum CatEvent
    {
        OnMerge
    }

    [RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
    public class Cat : MonoBehaviour , ITimedEntity , IDraggable , IPersistent < PersistentData >
    {
        public GameObject CurrencyParticle;
        public List < AudioClip > catSounds;
        public List < AudioClip > catHissingSounds;
        public AudioClip coinSound;
        public LayerMask collisionLayers;
        public LayerMask mergeLayers;
        private CatBlueprint _blueprint;

        private float currentProduction = 0f;

        public float ProductionPerSecond { get { return _blueprint.ProductionPerSecond; } }

        public Rigidbody dragbody
        { 
            get
            { 
                if (this == null)
                    return null;

                if (gameObject == null)
                    return null;

                return GetComponent < Rigidbody >(); 
            } 
        }

        public void OnSpawned()
        {
            CatBlueprint blueprint = CatDex.GetBlueprint(name);

            if (blueprint == null)
                return;

            _blueprint = blueprint;
        }

        public Vector3 midPoint
        { 
            get { return new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z); } 
        }

        public PersistentData Data
        { 
            get
            { 
                PersistentData persistentData = new PersistentData(_blueprint.Name, _blueprint.Location, transform.position);
                persistentData.InstanceID = transform.GetInstanceID();
                return persistentData;
            } 
        }

      

        protected Cat _mergableCat;

        protected bool _enabled = true;

        public bool Enabled { get { return _enabled; } }

        public void Start()
        {
        }

        public void Tick()
        {
            currentProduction += _blueprint.ProductionPerSecond;

            if (currentProduction >= _blueprint.ProductionMaxUpdate)
            {

                AudioCenter.PlayClip(catSounds[Random.Range(0, catSounds.Count)], 0.3f);
                UpdateCurrency(currentProduction);
                currentProduction = 0;
            }

        }

        public void UpdateCurrency(float amount)
        {
            if (!gameObject.activeSelf)
            {
                EventManager.Publish(this, ScoreEvent.Gold, amount);
                return;
            }

            AudioCenter.PlayClip(coinSound, 0.2f);

            Transform particle = FXManager.Spawn("CoinFX", new Vector3(transform.position.x - 0.5f, 
                                         transform.position.y + 0.5f, transform.position.z));


            foreach (var label in particle.GetComponentsInChildren<Text> ())
            {
                label.text = string.Format("+{0:N0}", amount);
            }

            EventManager.Publish(this, ScoreEvent.Gold, amount);

            particle.DOMove(new Vector3(particle.position.x, 
                    particle.position.y + 1f, particle.position.z), 0.6f).
            OnComplete 
            (
                () =>
                {
                    FXManager.Despawn(particle);
                }
            );
        }

        public void OnTouch()
        {
            AudioCenter.PlayClip(catHissingSounds[Random.Range(0, catHissingSounds.Count)], 0.5f);

            SendMessage("OnTouched", SendMessageOptions.DontRequireReceiver);
        }

        public void OnMerge(Vector3 midpoint, System.Action OnComplete = null)
        {
            FXManager.Spawn("CatMergeFX", midpoint.Add(-1f, 0.5f, 0f), 2f);

            transform.DOMove(midpoint, 0.1f).OnComplete 
            (
                () =>
                {
                    if (OnComplete != null)
                        OnComplete.Invoke();
                    
                    EventManager.Publish(this, RoomEvent.OnRemoveFromRoom, transform);

                }
            );

           
        }

        public void OnRelease()
        {

            if (_mergableCat != null)
            {
                Vector3 midpoint = _mergableCat.transform.position.Midpoint(transform.position);

                OnMerge(midpoint, () =>
                    {
                        CatBlueprint blueprint = CatDex.GetBlueprint(_blueprint.ID + 1);

                        PersistentData spawnData = new PersistentData(blueprint.Name, blueprint.Location, transform.position);

                        EventManager.Publish(this, CatEvent.OnMerge, blueprint);
                        EventManager.Publish(this, RoomEvent.OnAddToRoom, spawnData, -1);
                    });

                _mergableCat.OnMerge(midpoint);
            }
            else
            {
                SendMessage("OnReleased", SendMessageOptions.DontRequireReceiver);
            }

        }

        private void Update()
        {
            Vector3 position = transform.position;

            if (position.x <= 2f)
                position.x = 2f;
            if (position.x >= 7f)
                position.x = 7f;

            if (position.z >= -1.5f)
                position.z = -1.5f;
            if (position.z <= -7.5f)
                position.z = -7.5f;

            transform.position = position;
        }

        public void OnDrag(Vector3 position)
        {
            if (Drag3DHandler.current == Drag3DHandler.Current.Drag && Drag3DHandler.currentObject != this)
                return;

            if (position.y <= 1f)
            {
                position.x += position.y - 1f;
                position.y = 1f;
            }

            if (position.y >= 2f)
            {
                position.x += position.y - 2f;
            }

            transform.position = position;
            _mergableCat = null;

            RaycastHit[] hits = Raycast.CursorAll(mergeLayers, 1f);

            foreach (var info in hits)
            {
                if (info.transform == null || info.transform == transform)
                {
                    continue;
                }


                Cat cat = info.transform.GetComponent < Cat >(); // assume it has a cat script

                if (cat == null || cat.Data == null || Data == null || !name.Equals(cat.name))
                {
                    continue;
                }

                _mergableCat = cat;

                break;
            }
          
        }

    }
}