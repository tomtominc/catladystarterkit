﻿using UnityEngine;
using System.Collections;
using Framework.Utilities;

public class AudioCenter : Singleton<AudioCenter>
{
    private AudioSource _audioSource;
    private Vector3 _position;

    private void Awake()
    {
        _audioSource = GetComponent < AudioSource >();
        _position = Camera.main.transform.position;
    }

    public static void PlayClip(AudioClip clip, float volume)
    {
        Instance._audioSource.PlayOneShot(clip, volume);
    }
}
