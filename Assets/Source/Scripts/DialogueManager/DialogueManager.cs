﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using DoozyUI;
using UnityEngine.UI;
using System;

namespace Framework.Core
{
    public enum DialogueAction
    {
        SendDialogue
    }

    [System.Serializable]
    public class Dialogue
    {
        public string element;
        public string title;
        [TextArea]
        public string message;
    }

    public class DialogueSpeach
    {
        public List < Dialogue > dialogues;
        public Action onfinished;
        public Action onStart;
    }

	public class DialogueManager : Loader 
	{
        private bool _clicked = false;

        public override IEnumerator Load()
        {
            EventManager.Subscribe ( DialogueAction.SendDialogue , SendDialogue );
            yield return null;
        }

        private void SendDialogue ( IMessage message )
        {
            DialogueSpeach dialogueSpeach = (DialogueSpeach)message.Data;

            Queue < Dialogue > dialogueActions = new Queue < Dialogue > ( dialogueSpeach.dialogues );

            StartCoroutine ( SendDialogueIterator ( dialogueActions , dialogueSpeach.onStart, dialogueSpeach.onfinished ) );
        }

        private IEnumerator SendDialogueIterator ( Queue < Dialogue > dialogues , Action onStart, Action onfinished )
        {
            onStart.Invoke ();

            while ( dialogues.Count > 0 )
            {
                var dialogue = dialogues.Dequeue ();

                UIManager.ShowNotification ( dialogue.element, -1 , true , dialogue.title, dialogue.message );

                while ( _clicked == false ) yield return null;

                _clicked = false;
            }

            onfinished.Invoke ();
        }

        public void DoNext ()
        {
            _clicked = true;
        }


	}
}