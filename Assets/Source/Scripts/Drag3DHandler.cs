﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using System.Collections.Generic;

namespace Framework.Core
{
    public interface IDraggable
    {
        bool Enabled { get; }

        Rigidbody dragbody { get; }

        void OnTouch();

        void OnDrag(Vector3 position);

        void OnRelease();
    }

    public enum DragAction
    {
        Enable
    }

    public enum DragHandles
    {
        Touch,
        TouchAndDrag
    }

    public class Drag3DHandler : MonoBehaviour
    {
        public LayerMask dragLayer;
        public DragHandles dragHandles;
        private IDraggable dragBehaviour;

        private Vector3 screenPoint;
        private Vector3 offsetPosition;

        public enum Current
        {
            None,
            Drag
        }

        public static Current current;
        public static IDraggable currentObject;

        private void Awake()
        {
            EventManager.Subscribe(GameStateEvent.OnChangeGameState, OnLoadGameState);
            EventManager.Subscribe(DragAction.Enable, Enable);
        }

        private void Enable(IMessage message)
        {
            DragHandles dh = (DragHandles)message.Data;

            dragHandles = dh;
        }

        private void OnLoadGameState(IMessage message)
        {
            GameState state = (GameState)message.Data;

            if (state == GameState.NewGame)
            {
                dragHandles = DragHandles.Touch;
            }
            else
            {
                dragHandles = DragHandles.TouchAndDrag;
            }
        }

        private void Update()
        {
            if (Input.GetMouseButtonUp(0) && dragBehaviour != null)
            {
                dragBehaviour.OnRelease();

                if (dragBehaviour.dragbody != null)
                    dragBehaviour.dragbody.isKinematic = false;

                dragBehaviour = null;
                currentObject = null;
                current = Current.None;
            }

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit = Raycast.Cursor(dragLayer);

                if (hit.transform != null && hit.transform.GetComponent < IDraggable >() != null)
                {
                    dragBehaviour = hit.transform.GetComponent < IDraggable >();
                    currentObject = dragBehaviour;

                    if (!dragBehaviour.Enabled)
                        return;

                    dragBehaviour.OnTouch();

                    if (dragBehaviour != null && dragBehaviour.dragbody != null)
                    {
                        screenPoint = Camera.main.WorldToScreenPoint(dragBehaviour.dragbody.transform.position);
                        offsetPosition = dragBehaviour.dragbody.transform.position - Camera.main.ScreenToWorldPoint 
                            (new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
                    }
                   
                }
            }

            if (dragHandles != DragHandles.TouchAndDrag)
                return;

            if (dragBehaviour != null && dragBehaviour.dragbody != null && dragBehaviour.Enabled)
            {
                current = Current.Drag;

                dragBehaviour.dragbody.isKinematic = true;

                Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
                Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offsetPosition;

                dragBehaviour.OnDrag(currentPosition);
            }
        }
    }
}