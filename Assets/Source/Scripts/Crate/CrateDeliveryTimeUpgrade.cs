﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Framework.EditorTools;
using Framework.Core.EventSystem;

namespace Framework.Core
{
	public class CrateDeliveryTimeUpgrade : MonoBehaviour 
	{
        [ButtonAttribute("Reset Upgrade", "ResetUpgrade" ) ]
        public Text priceLabel;
        public Text levelLabel;
        public Text secondsLabel;
        public Button upgradeButton;
        public GameObject GUICrate;
        public List < Texture > CrateUpgradeTextures;
        public int MaxUpgrade = 5;
        public int startingPrice = 1000;

        public bool isDeliveryUpgrade = false;
        public TimeManager timeManager;

        private void Start ()
        {
            if ( isDeliveryUpgrade )
            {
                SetDeliveryUpgrade ();
            }
            else
            {
                SetUpgrade ();
            }

        }

        public float CurrentDeliveryPrice
        {
            get { return startingPrice * Mathf.Pow (10.15f, CurrentDeliveryUpgrade - 11 ); }
        }

        public float CurrentUpgradePrice
        {
            get { return startingPrice * Mathf.Pow (10.15f, CurrentCrateUpgrade ); }
        }

        public int CurrentUpgradeValue
        {
            get { return isDeliveryUpgrade ? CurrentDeliveryUpgrade : CurrentCrateUpgrade; }
        }

        public int CurrentCrateUpgrade
        {
            get 
            {
                return PlayerPrefs.GetInt ( "CrateUpgrade", 0 );
            }
            set 
            { 
                PlayerPrefs.SetInt ( "CrateUpgrade", (value > MaxUpgrade) ? MaxUpgrade : value );
            }
        }

        public int CurrentDeliveryUpgrade
        {
            get 
            {
                return PlayerPrefs.GetInt ( "DeliveryUpgrade", 12 );
            }
            set 
            { 
                PlayerPrefs.SetInt ( "DeliveryUpgrade", (value <= MaxUpgrade) ? MaxUpgrade : value );
            }
        }

        public float Gold
        {
            get { return StatsManager.Instance.Gold; }
        }
        public void ResetUpgrade ()
        {
            CurrentDeliveryUpgrade = 12;
            CurrentCrateUpgrade = 0;
        }

        public Texture MainTexture
        {
            get { return CrateUpgradeTextures [ CurrentCrateUpgrade ]; }
        }

        private void SetDeliveryUpgrade ()
        {
           
            priceLabel.text = CurrentDeliveryPrice.ToString ("N0");

            levelLabel.text = string.Format ( "{0}s -> {1}s", CurrentUpgradeValue , CurrentUpgradeValue - 1  );

            timeManager.crateProductionInSeconds = (float)CurrentUpgradeValue;

            if ( CurrentDeliveryUpgrade <= MaxUpgrade) 
            {
                upgradeButton.enabled = false;
                priceLabel.text = "MAXED";
                levelLabel.text = string.Format ( "({0}s) Max Level Reached", CurrentUpgradeValue );
            }

        }

        private void SetUpgrade ()
        {
            GUICrate.GetComponent < Renderer > ().sharedMaterial.mainTexture = MainTexture;

            priceLabel.text = CurrentUpgradePrice.ToString ("N0");

            levelLabel.text = string.Format ( "{0} lvl -> {1} lvl Cats", CurrentCrateUpgrade + 1 , CurrentCrateUpgrade + 2  );

            if ( CurrentCrateUpgrade >= MaxUpgrade ) 
            {
                upgradeButton.enabled = false;
                priceLabel.text = "MAXED";
                levelLabel.text = string.Format ("({0} lvl) Max Level Reached", CurrentCrateUpgrade + 1 );
            }

        }

        public void UpgradeDeliverySpeed ()
        {
            if ( CurrentDeliveryPrice > Gold ) return;

            EventManager.Publish ( this , ScoreEvent.Gold, -CurrentDeliveryPrice );

            CurrentDeliveryUpgrade--;

            SetDeliveryUpgrade ();
        }

        public void UpgradeCrate ()
        {
            if ( CurrentUpgradePrice > Gold ) return;

            EventManager.Publish ( this , ScoreEvent.Gold, -CurrentUpgradePrice );

            CurrentCrateUpgrade++;

            SetUpgrade (); 
        }
	}
}