﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Framework.Core.EventSystem;
using RutCreate.LightningDatabase;

namespace Framework.Core
{
    public enum CrateEvent
    {
        OnSmashed,
        OnSpawned
    }

    public class Crate : MonoBehaviour , IDraggable , IPersistent < PersistentData >
    {
        public GameObject explodeParticle;
        public AudioClip explodeAudio;
        public float explodeVolume;

        private int currentCrate = 0;

        protected bool _enabled = true;

        public bool Enabled { get { return _enabled; } }

        public class EventData
        {
            public GameObject gameObject;
            public int catGeneration;
        }

        public Rigidbody dragbody
        { 
            get
            { 
                if (this == null)
                    return null;

                return GetComponent < Rigidbody >(); 
            } 
        }

        public void OnSpawned()
        {
            Upgrade upgrade = PersistentManager.Load < Upgrade >("Crate Quality", null);

            if (upgrade != null)
            {
                currentCrate = (int)upgrade.current;
            }

            EventManager.Publish(this, CrateEvent.OnSpawned, new EventData() { gameObject = gameObject });
        }

        public void OnTouch()
        {
            FXManager.Spawn("Explosion", new Vector3(transform.position.x - 1f, transform.position.y + 0.5f, transform.position.z), 1f);

            int generation = Random.Range(1, currentCrate + 1);

            EventManager.Publish(this, RoomEvent.OnRemoveFromRoom, transform);

            CatBlueprint blueprint = CatDex.GetBlueprint(generation);

            //fix
            PersistentData spawnData = new PersistentData(blueprint.Name, blueprint.Location, transform.position);

            EventManager.Publish(this, RoomEvent.OnAddToRoom, spawnData, -1);
           
            AudioCenter.PlayClip(explodeAudio, explodeVolume);

            EventManager.Publish(this, CrateEvent.OnSmashed, new EventData() { gameObject = gameObject, catGeneration = generation });
        }

        public void OnRelease()
        {
            
        }

        public void OnDrag(Vector3 position)
        {
            
        }

        public PersistentData Data
        {
            get
            {
                return new PersistentData()
                { 
                    PrefabName = "Crate", 
                    WorldPosition = transform.position,
                    PoolName = Location.House.ToString(),
                    InstanceID = transform.GetInstanceID()
                };
            }
        }
    }
}