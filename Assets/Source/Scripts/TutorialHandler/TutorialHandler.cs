﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using HardCodeLab.TutorialMaster;
using UnityEngine.UI;
using DoozyUI;
using DG.Tweening;

namespace Framework.Core
{
    public class TutorialHandler : Loader
    {
        public Image background;
        public UIElement textBox;
        public RectTransform arrow;
        public Button nextButton;
        public Vector2 arrowOffset = new Vector2(120f, -20f);

        public List <GameObject> endOfTutorialEnableObjects;

        public GameObject catMeter;
        public GameObject spawnParticle;
        private Vector2 arrowOrigin;
        private Tween _arrowMutateTutorialTween;

        public override IEnumerator Load()
        {
            EventManager.Subscribe(GameStateEvent.OnChangeGameState, OnLoadGameState);
            EventManager.Subscribe(CrateEvent.OnSmashed, OnCrateSmashed);
            EventManager.Subscribe(CrateEvent.OnSpawned, OnCrateSpawned);
            EventManager.Subscribe(CatEvent.OnMerge, OnMergeCat);

            arrowOrigin = arrow.anchoredPosition;

            yield return null;
        }

        private void OnLoadGameState(IMessage message)
        {
            GameState state = (GameState)message.Data;

            if (state == GameState.NewGame)
            {
                StartOnboardingProcess();
            }
            else
            {
                textBox.Hide(false);
                background.DOFade(0.0f, 0.5f);
                nextButton.interactable = false;
                catMeter.SetActive(true);

                foreach (var go in endOfTutorialEnableObjects)
                {
                    go.SetActive(true);
                }
            }
        }

        private void StartOnboardingProcess()
        {
            tutorial.Start(0);
        }

        public void Next()
        {
            frame.Next();
        }

        public void OnFrameEnter()
        {
            int id = frame.id;

            TryShowDialogue();

            CheckForEvent();
        }

        public void OnTutorialStart()
        {
            int id = tutorial.id;

            TryShowDialogue();
        }

        public void OnTutorialEnd()
        {
            int id = tutorial.id;

            FinishTutorial();
        }

        public void FinishTutorial()
        {
            textBox.Hide(false);
            background.DOFade(0.0f, 0.5f);
            nextButton.interactable = false;

            if (tutorial.id == 0)
            {
                foreach (var go in endOfTutorialEnableObjects)
                {
                    go.SetActive(true);
                }

                EventManager.Publish(this, GameStateAction.ChangeState, GameState.OnGoingGame);
            }
        }



        public void TryShowDialogue()
        {
            if (tutorial.tmObject.frame_data.useText)
            {
                textBox.Show(false);
                nextButton.interactable = true;
            }
            else
            {
                textBox.Hide(false);
                nextButton.interactable = false;
            }
        }

        private void MoveArrow(Vector2 position)
        {
            arrow.DOAnchorPos(position + arrowOffset, 1f);
        }

        public void CheckForEvent()
        {
            if (tutorial.id == 0 && frame.id == 7)
            {
                nextButton.interactable = false;

                RectTransform anchor = textBox.GetComponent < RectTransform >();

                anchor.DOAnchorPos(new Vector2(anchor.anchoredPosition.x, anchor.anchoredPosition.y + 100f), 0.5f).OnComplete 
                (
                    () =>
                    {
                        GameObject clone = Instantiate < GameObject >(spawnParticle);

                        clone.transform.position = catMeter.transform.position;

                        Destroy(clone, 4f);

                        catMeter.SetActive(true);

                        nextButton.interactable = true;
                    }
                );
            }

            if (tutorial.id == 0 && frame.id == 11)
            {
                nextButton.interactable = false;

                Cat[] cats = GameObject.FindObjectsOfType < Cat >();
                Canvas canvas = UIManager.GetUiContainer.GetComponent < Canvas >();

                Vector2 p0 = canvas.WorldToCanvas(cats[0].transform.position);
                Vector2 p1 = canvas.WorldToCanvas(cats[1].transform.position);

                arrow.anchoredPosition = p0 + arrowOffset;

                _arrowMutateTutorialTween = arrow.DOAnchorPos(p1 + arrowOffset, 1f).SetLoops(-1, LoopType.Yoyo);

                EventManager.Publish(this, DragAction.Enable, DragHandles.TouchAndDrag);
            }
        }

        public void OnMergeCat(IMessage message)
        {
            if (tutorial.id == 0 && frame.id == 11)
            {
                _arrowMutateTutorialTween.Kill();

                MoveArrow(arrowOrigin);

                Next();
            }
        }

        public void OnCrateSpawned(IMessage message)
        {
            if (tutorial.id == 0 && frame.id == 5)
            {
                Crate.EventData data = (Crate.EventData)message.Data;

                Canvas canvas = UIManager.GetUiContainer.GetComponent < Canvas >();

                Vector2 position = canvas.WorldToCanvas(data.gameObject.transform.position);

                MoveArrow(position);
            }
        }

       

        public void OnCrateSmashed(IMessage message)
        {
            if (tutorial.id == 0 && frame.id == 5)
            {
                Next();

                MoveArrow(arrowOrigin);
            }

            if (tutorial.id == 0 && frame.id == 8)
            {
                Next();
            }

        }

      

    }
}