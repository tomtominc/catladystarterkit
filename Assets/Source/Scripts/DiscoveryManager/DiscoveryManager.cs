﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using Framework.Core.EventSystem;
using RutCreate.LightningDatabase;
using DoozyUI;
using UnityEngine.UI;

public class DiscoveryManager : Loader
{
    [SerializeField] private CatDexIcon _catDexIcon;
    [SerializeField] private Text _label;

    public override IEnumerator Load()
    {
        EventManager.Subscribe(GameStateEvent.OnChangeGameState, OnChangeGameState);
        EventManager.Subscribe(RoomEvent.OnAddToRoom, OnAddToRoom);

        _catDexIcon.Init();

        yield break;
    }

    private void OnChangeGameState(IMessage message)
    {
        GameState state = (GameState)message.Data;

        if (state == GameState.NewGame)
        {
            CatDex.ResetCatSavedData();
        }

    }

    private void OnAddToRoom(IMessage message)
    {
        PersistentData data = (PersistentData)message.Data;

        bool discovered = CatDex.HasBeenDiscovered(data.PrefabName);

        if (discovered)
            return;

        CatDex.SetDiscovered(data.PrefabName, true);

        _label.text = data.PrefabName.SplitCamelCase();
        _catDexIcon.Set(CatDex.GetBlueprint(data.PrefabName));

        UIManager.ShowUiElement("CatDiscoveredUI");

        StartCoroutine(HideUI());

    }

    private IEnumerator HideUI()
    {
        yield return new WaitForSeconds(3f);
        UIManager.HideUiElement("CatDiscoveredUI");
    }
}
